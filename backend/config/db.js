// Aqui é o mongoose, necessário para se conectar com o mongoDB.
const mongoose = require('mongoose')
// Biblioteca de configuração do ambiente, para configurar a forma como a conexão será feita
const config = require('config')

// Ela está pegando a chave de acesso, o mongoURI que está la no default.json
const db = config.get('mongoURI')

// O mongoDB tem um tempo de resposta dele, por isso sempre será um método assíncrono
const connectDB = async () => {
    try {
        //Aqui que é feita a chamada pro mongoDb(mongoose.connect), utilizando a URI que está no default.json(db)
        await mongoose.connect(db, {
            useNewUrlParser: true, // As regras não foram explicadas
            useCreateIndex: true,
            useFindAndModify: false,
            useUnifiedTopology: true
        });
        console.log("Puxe um banco, vamos jogar dados") // Mostra no console seta tudo certo, no caso aqui, conectou
    }catch(err) {
        console.error(err.message); // aqui pega o erro que aparecer e posta no
console
        process.exit(1) // Aqui ele sai
    }
};

// Agora precisamos exportar ela pro server.js conseguir se conectar
module.exports = connectDB;
