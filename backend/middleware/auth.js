const jwt = require('jsonwebtoken');
const config = require('config');

module.exports = function (req, res, next) {
    const token = req.header('x-auth-token');

    if (!token) {
        return res.status(401).json({ msg: 'No token, authorization denied'});
    }

    try{
        jwt.verify(token, config.get('jwtSecret'), (error, decoded) => {
            if (error) {
                return res.status(401).json({msg: 'Token is not valid'});
            }
            req.user = decoded.user;
            if (req.baseUrl == '/profile' && decoded.user.nome == false){
                return res.status(403).json({ msg: 'Legionário inativo'})
            }
            next();
        })
    }catch(err) {
        console.error('Something wrong with auth middleware');
        res.status(500).json({ msg: 'Server Error'});
    }
}