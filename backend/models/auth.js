// Aqui é um dos programas responsáveis por fazer a autenticacao do jwt

const express = require('express') // pra fazer a rota
const router = express.Router() // pra usar os métodos das rotas
const User = require('../../models/user') // aqui é importante, nele que terão os objetos que estamos trabalhando
const bcrypt = require('bcryptjs') // importando o bcrypt pra cuidar das encriptações de senhas
const jwt = require('jsonwebtoken') // jwt pra fazer autenticacao
const config = require('config') // config aqui é pra configurar a autenticacao, ou seja, o jwt
const { check, validationResult } = require('express-validator') // pra fazer os checks


// Nossa autenticacao será pelo método POST, mas pode ser qualquer outro método
router.post('/',[
    check('nome', 'O nome, combatente').notEmpty(), // usa o express-valitador pra ver se o campo nome não está vazio, se estiver, manda a msg
    check('senha', 'Falta a senha').exists() // checha se o campo senha existe,caso contrário, manda mensagem Falta senha
], async(req, res) => { // tudo que envolve Mongo/JWT precisa ser assíncrona pra poder coletar a resposta lá e trazer pra cá
    const errors = validationResult(req) // Os erros gerados pelo método validationResult do express-validator são armazenados na const erros
    if (!errors.isEmpty()) { // Caso errors não esteja vazio, ou seja, tenha qualquer erro.
        return res.status(400).json({ errors: errors.array()}) // envia a mensagem de erro contendo os erros em forma de array
    }
    const { nome, senha} = req.body // Crio as const que vão receber os campos nome e senha no corpo da requisicao, normal

    try{// Agora o user precisa ser await porque ele está acessando nosso banco. Com o método findOne pra encontrar o nome, percorrendo tudo o que eu
        let user = await User.findOne({ nome }).select('nacionalidade nome senha idade endereco') // puser em select, todos os campos e trazendo de volta
        if (!user) { // Se nao encontrar o que eu busquei
            return res.status(404).json({ errors: [{ msg: 'Legionário não existe'}]}) // manda mensagem de que nao foi encontrado em formato json
        }else{ // criamos uma const isMatch pra saver se as senas estão batendo, com um método do bcrypt de comparar a senha no banco com a que
            const isMatch = await bcrypt.compare(senha, user.senha) // pusemos no postman
            if (!isMatch) { // se forem diferentes
                return res.status(400).json({ errors: [{ msg: 'Senha Incorreta'}
]}) // manda a mensagem que tá incorreta
            }else{
                if (user.nome == false){ // se o nome não bater
                    return res.status(403).json({ errors: [{ msg: 'Legionário inativo'}]})
                }
                const payload = { // criamos essa const pra conectarmos com o models/user.js, ela está acessando lá e pegando cada um dos dados abaixo
                    user: {
                        id: user.id,
                        nome: user.nome,
                        senha: user.senha
                    }
                }
                // Aqui que começa de fato a autenticacao, com o método sign que recebe 4 parametros:
                // o primeiro é o que estamos querendo verificar, o segundo é anossa configuração que está em jwtSecret em default.json p fazer
                // a comparacao, o terceiro é quando expira esse token, o quarto verifica se o que foi enviado no header do postman está batendo,
                // se der erro, manda a mensagem de erro. Senao, envia o token e autentica
                jwt.sign( payload, config.get('jwtSecret'), { expiresIn: '1h'},
                (err, token) => {
                    if(err) throw err;
                    res.json({ token });
                }
                );
            }
        }

    }catch(err) {
        console.error(err.message)
        res.status(500).send('Server error')
    }
})

module.exports = router