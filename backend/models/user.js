// Aqui estamos importando o moongose, depois de ter dado o npm install mongoose. Ele irá nos conectar com o nosso banco de dados
const mongoose = require('mongoose')

//Aqui no UserSchema, nós estamos criando o modelo que será utilizado no nosso banco de dados, o nosso esquema, nosso formato
// No meu caso eu criei alguns campos e decididi que todos seriam obrigatórios,menos o endereço.
// Para aprender mais sobre as possibilidades e complexidades do User Schema, precisaríamos olhar a documentação
const UserSchema = new mongoose.Schema({
    nacionalidade: {
        type: String,
        required: true
    },
    nome : {
        type: String,
        required: true
    },
    senha : {
        type : String,
        required: true
    },
    idade: {
        type : String,
        required : true
    },
    endereco : {
        type: String,
        required: false
    }

})

// perceba que aqui estamos exportando como model, com um nome de user que é o que vai aparecer no banco de dados, no Atlas.
//Dessa forma, você consegue decidir o que é necessário, quando o usuário estiver preenchendo. Campos obrigatórios, por exemplo.
module.exports = mongoose.model('user', UserSchema)
