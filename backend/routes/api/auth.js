// Importa o jwt, que é o nosso autenticador para dar mais segurança ao projetoconst jwt = require('jsonwebtoken');
// importa o config, que vamos utilizar para configurar o jwt. Parecido com o que fizemos com mongoose
const config = require('config');

// Aqui em vez de fazer a funcao e exportar depois, simplesmente estamos fazendo ela já pronta pra ser exportada
module.exports = function (req, res, next) {    //o token está recebendo um header(daqueles vários que tem no post), no caso o de nome x-auth-token
    const token = req.header('x-auth-token')    // Caso não tenha nenhum token lá
    if (!token) {
        return res.status(401).json({ msg: 'No token, authorization denied'}) // retorna a mensagem que tá sem token
    }

    try{ // o método verify da biblioteca do jwt, pede 3 argumentos. O primeiroé o token, o segundo é a configuração que passamos la no default.json
        // desse token, e o terceiro ele pega dois parametros. Um de erro e o outro de que decodificou e ta tudo certo
       jwt.verify(token,config.get('jwtSecret'), (error, decoded) => {
                       if (error) { // se encontrar erro, manda uma mensagem de erro
                return res.status(401).json({ msg: 'Token is not valid'});
            }
            req.user = decoded.user; // o usuário decodificado que voce mandou no post, agora vai aparecer na tela com o request
            // if (req.baseUrl == '/profile' && decoded.user.is_admin == false){  ### Essa parte não vou usar no meu programa mas,
            //     return res.status(403).json({msg: 'user is not admin' }); ### ele chega se tem no baseUrl o /profile e se o usuário
            // } ### é admin, no caso se for falso ele envia mensagem de que o usuário não é adm
            next(); // Aqui é muito importante, porque se não tiver esse next, o programa não sai, fica direto preso no ' ('/), auth, ' no auth.js da routes/api
        })
    }catch(err){
        console.error('something with auth middleware'); // nada de novo por aqu
i
        res.status(500).json({msg: 'Server Error'});
    }
}
