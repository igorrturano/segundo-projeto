// Relembrando que o express é um framework flexível e minimalista, que possui uma série de funcionalidades para ajudar no desenvolvimento web
const express = require('express')
// Estamos pegando o método Router() da biblioteca express, para fazermos os métodos GET,POST,ETC. Tem mais funcionalidades
// Ficou meio vago pra mim também usá-lo se só com express já servia. Mas tem mais funcionalidades, por enquanto é apenas uma boa prática
const router = express.Router();
const User = require('../../models/user')
// Express validator(npm install express-valitador), serve para que possamos fazer a verificação do input, pra por exemplo, um e-mail conter @ e .
// Estão entre {chaves} porque estamos pegando as funcionalidades check e ValidationResult do express-validator
const { check, validationResult } = require('express-validator')
// Importa o bcrypt
const bcrypt = require('bcryptjs')
const auth = require('../../middleware/auth')

// TO DO:
// fazer o express validator [], try catch

router.get('/',auth, async (req, res, next) => { // Para trabalhar com o banco de dados precisa usar o metodo async
    try{
     let user = await User.find({}) // Aqui estamos acessando uma funcao do mongo que vai nos retornar um objeto
      res.json(user) // Aqui ele manda pro browser no formato json(Um dos motivos pelo qual utilizamos o bodyparser no server.js)
    }catch(err){
        console.error(err.message)
        res.status(500).send({"error": "server error"})
    }
})

// Um método GET pra encontrar o usuário no banco de dados, utilizando o ID como parâmetro de busca
router.get('/:userId', async (req, res, next) => {
    try{
        const id = req.params.userId // estamos falando que a const id recebe oID que está na requisição da url
        const user = await User.findOne({_id : id}) // Aqui estamos acessando  o banco de dados e encontrando o usuário com o método FindOne
        if (user) { // Se o usuário existir,
            res.json(user) // mostra o usuário
        }else{
            res.status(404).send({"error": "user not found"}) // se nao, envia error 404 não encontrado
        }
    }catch(err){
        console.error(err.message)
        res.status(500).send({"error":"Server Error"}) // Método catch pro casode algo der errado
    }
})


//post vai trabalhar primeiro com a desestruturacao do que foi inserido no corpo da requisicao
// Depois vou armazenar todos os dados da requisicao numa variavel que chamei usuario
router.post('/', [
    check('nacionalidade', 'campo vazio').not().isEmpty(), // Esses colchetes são nosso express-validator, aqui nós colocamos
    check('nome', 'campo vazio').not().isEmpty(), // Algumas regras pra ele validar. No meu caso eu só pedi pra não liberar campos vazios
    check('senha', 'campo vazio').not().isEmpty(),
    check('idade', 'campo vazio').not().isEmpty(),
    ], async (req, res, next) => {
    try{
        let { nacionalidade, nome, senha, idade, endereco } = req.body

        const errors = validationResult(req)

        if (!errors.isEmpty()){
            return res.status(400).json({errors:errors.array()})
        }else{
            let usuario = new User({nacionalidade, nome, senha, idade, endereco}
)

            const salt = await bcrypt.genSalt(8)
            usuario.senha = await bcrypt.hash(senha, salt)

            await usuario.save()

            if (usuario.id) { // verifica se está dentro do banco de dados
                res.json(usuario)
            }
        }
    }catch(err){
        console.error(err.message)
        res.status(500).send({"Error" : "Server Error"})
    }
})

// PUT é o método que usamos pra atualizar os dados, cuidado porque nele substitui tudo, mesmo que não queira
router.put('/:userId', async (req, res, next) => {
    try {
        const errors = validationResult(req) // armazena os erros do método validationResult na variavel errors
        if (!errors.isEmpty()){ // Se a variavel que armazena os erros nao estiver vazia, ou seja, conter erros
            return res.status(400).json({errors : errors.array()}) // envia os erros junto com o status 400(bad request)
        }
        const id = req.params.userId // armazena o parametro passado pelo url na const id
        let { nacionalidade, nome, senha, idade, endereco } = req.body // pega o que escrevemos no corpo da requisicao e atribui as variaveis
        let update = { nacionalidade, nome, senha, idade, endereco }; // Vamos utilizar essa variável para armazenar os novos valores preenchidos

        const salt = await bcrypt.genSalt(8)
        update.senha = await bcrypt.hash(senha, salt)

        let user = await User.findOneAndReplace({_id : id}, update, {new : true}
) // Aqui buscamos pelo ID, substituímos o que encontramos pelo update
        // ou seja, o que acabamos de escrever. E no new: true, dizemos para manter o valor novo
        if (user) { // se existir a variavel user, ou seja, se tiver no banco de dados, mostra ele
            res.json(user)
        }else{
            res.status(404).send({"error":"user not found"}) // se  nao, manda erro usuario nao encontrado
        }
    } catch (err) { // caso nada acima de certo, ou seja, o try falhe:
        console.error(err.message) // envia o erro pro terminal pro backend ver
        res.status(500).send({"error":"Server Error"}) // envia o erro pro browser pro fronted ver
    }
})

router.patch('/:userId', [
    check('nacionalidade', "campo vazio").not().isEmpty()
], async (req, res, next) => {
try {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        res.status(400).send({ errors : errors.array()})
        return
    }
    const id = req.params.userId

    let bodyRequest = req.body

    if(bodyRequest.senha){
        const salt = await bcrypt.genSalt(8)
        bodyRequest.senha = await bcrypt.hash(bodyRequest.senha, salt)
    }

    const update = { $set: bodyRequest }
    const user = await User.findOneAndUpdate(id, update, {new: true})
    if (user) {
        res.json(user)
    }else{
        res.status(404).send({"error": "user not found"})
    }
} catch (error) {
    console.error(error.message)
    res.status(500).send({"Error": "Server Error"})
} // update
})

// pegar o paramatro, encontrar o id que foi passado e deletar
router.delete('/:userId', async (req, res, next) => { //delete
    try{
     const id = req.params.userId
     const user = await User.findByIdAndDelete({_id : id})
     if (user) {
         res.json(user)
     }else{
         res.status(404).send({"error": "user not found"})
     }
    }catch(err){
        console.error(err.message)
        res.status(500).send({"error" : "Server Error"})
    }
})


module.exports = router
