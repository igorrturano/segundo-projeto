// Primeiro de tudo, para se trabalhar no backend nós usamos o Node.js, que é umruntime enviroment,  ou seja,
// um ambiente em tempo de execução. Ele nos permite criar aplicações javascript multiplataforma direto do pc.

// Aqui estamos importando todas aquelas configurações do db.js para podermos fazer a conexão por aqui.
const connectDB = require('./config/db')
//Só que criar métodos CRUD é um trabalho enorme se feito do zero, para isso existem alguns frameworks que nos auxiliam.
// Assim não precisamos reinventar a roda. Aqui nós importamos o express após instalá-lo(npm install express)
const express = require('express')
// Aqui chamamos ele com a const app, que deterá suas funções para nos auxiliarcom códigos já implementados, economizando-nos tempo e trabalho
// Inclusive é com ele que vamos chamar o bodyparser
const app = express()
// O bodyparser vai receber a requisicao HTTP que foi enviada do browser pro servidor e ler tudo o que está dentro dele e traduzir em formato json.
// Sem isso, muitos bugs ocorreriam na hora da leitura, porque os dados estariam na sua forma bruta
var bodyParser = require('body-parser')
// Aqui estamos simplesmente definindo a porta(no caso 3000), e esse process.env significa que quando alguma aplicação(como o heroku) quiser usar
// nosso programa, ele pode fazê-lo usando uma porta de sua preferência
const PORT = process.env.PORT || 3001;

app.use(express.json())
// urlencoded extended true significa que você poderá visualizar objetos rico de detalhes em formato json. E também lê qs library(não sei oque é)
app.use(bodyParser.urlencoded({extended:true}))
// executa o pacote body-parser. OBS.: Depois da versão do express 4.16, não precisa + de bodyParser, exceto para coisas bem específicas. Então
// poderíamos substituir o urlencoded acima por app.use(express.urlencoded()) e o bodyparser.json abaixo por app.use(express.json())
app.use(bodyParser.json())

//Aqui é feita a conexão com o MongoDB
connectDB()

// app.use (function (req, res, next) {
//     var schema = (req.headers['x-forwarded-proto'] || '').toLowerCase();
//     if (req.headers.host.indexOf('localhost') < 0 && schema !== 'https') {
//         // request was via http, so redirect to https
//         res.redirect('https://' + req.headers.host + req.url);
//     }
//     next();
// });

// Simplesmente importa o user pra que o server possa rodá-lo. Repare que adicionamos um novo endereço "/user"
app.use('/user', require('./routes/api/user'))
app.use('/auth', require('./routes/api/auth'))

// Aqui ele acessa nossa porta pelo servidor local e em seguida envia uma mensagem pro terminal
app.listen(PORT, console.log(`Já somos ${PORT} legionários alistados`))